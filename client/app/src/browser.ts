import React from 'react';
import ReactDOM from 'react-dom';

import store from 'store';

import App from 'components/app/App';

const appNode = document.getElementById('app');

ReactDOM.render(React.createElement(App, {store}), appNode);
