import path from 'path';
import express from 'express'
// import React from 'react';
// import ReactDOMServer from 'react-dom/server';
import Handlebars from 'handlebars';

import layout from 'layout.handlebars';

// import App from 'components/app/App';

const app = express();

app.use(express.static(path.resolve(__dirname, 'public')));

app.get('*', async (req, res) => {
    // const app = ReactDOMServer.renderToString(
    //     React.createElement(App)
    // );
    const html = Handlebars.compile(layout);
    res.send(html({
        title: 'APP',
        bundle: '/js/app.js',
        app: '',
    }));
});

app.listen(3000, () => console.log('Running on port 3000...'));
