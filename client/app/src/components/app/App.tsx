import React from 'react';
import { Provider } from 'react-redux';

import { Store } from '@reduxjs/toolkit';

import Categories from 'components/categories/Categories';

export type AppProps = {
    store: Store;
}

const App = ({store}: AppProps) => {
    return (
        <Provider store={store}>
            <Categories />
        </Provider>
    );
}

export default App;
