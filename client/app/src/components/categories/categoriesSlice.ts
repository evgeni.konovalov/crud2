import {createSlice, createAsyncThunk} from '@reduxjs/toolkit';

interface Category {
    id: number;
    name: string;
    slug: string;
}

const fixture = [
    {
        id: 1,
        name: 'Cat1',
        slug: 'cat1'
    },
    {
        id: 2,
        name: 'Cat2',
        slug: 'cat2'
    }
]

export const fetchCategories = createAsyncThunk('categories/fetch', async () => {
    await new Promise(resolve => {
        setTimeout(resolve, 3000);
    });
    return fixture;
});

export const categoriesSlice = createSlice({
    name: 'categories',
    initialState: {
        isFetching: false,
        list: [] as Category[]
    },
    reducers: {},
    extraReducers: builder => {
        builder
        .addCase(fetchCategories.pending, state => {
            state.isFetching = true;
        })
        .addCase(fetchCategories.fulfilled, (state, action) => {
            state.list = action.payload;
            state.isFetching = false;
        });
    }
});

export default categoriesSlice.reducer;
