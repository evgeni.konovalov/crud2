const path = require('path');

module.exports = {
    entry: './src/browser.ts',
    output: {
        path: path.resolve(process.cwd(), 'build', 'public', 'js'),
        filename: 'app.js'
    },
    resolve: {
        modules: [
            path.resolve(process.cwd(), 'src'), 
            'node_modules'
        ],
        extensions: ['.js', '.ts', '.jsx', '.tsx']
    },
    module: {
        rules: [
            {
                test: /\.tsx?$/,
                use: 'ts-loader',
                exclude: /node_modules/,
            },
        ],
    },
    optimization: {
        minimize: false
    },
};
