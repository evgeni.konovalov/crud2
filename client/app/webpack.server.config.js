const path = require('path');
const NodeExternals = require('webpack-node-externals');

module.exports = {
    entry: './src/server.ts',
    target: 'node',
    output: {
        path: path.resolve(process.cwd(), 'build'),
        filename: 'server.js'
    },
    resolve: {
        modules: [
            path.resolve(process.cwd(), 'src'), 
            'node_modules'
        ],
        extensions: ['.js', '.ts', '.jsx', '.tsx']
    },
    module: {
        rules: [
            {
                test: /\.tsx?$/,
                use: 'ts-loader',
                exclude: /node_modules/,
            },
            {
                test: /\.handlebars$/,
                use: 'raw-loader',
            },
        ],
    },
    optimization: {
        minimize: false
    },
    externals: [NodeExternals()]
};
